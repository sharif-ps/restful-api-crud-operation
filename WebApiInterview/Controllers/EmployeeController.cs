﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApiInterview.Models;

namespace WebApiInterview.Controllers
{
    public class EmployeeController : ApiController
    {
        public IEnumerable<Employee> getAll()
        {

            using (EmployeeRecordsEntities entities = new EmployeeRecordsEntities())
            {
                return entities.Employees.ToList();
            }
        }

        public Employee getById(int id)
        {

            using (EmployeeRecordsEntities entities = new EmployeeRecordsEntities())
            {
                return entities.Employees.FirstOrDefault(e => e.ID == id);
            }
        }

        public void store(Employee employee)
        {

            using (EmployeeRecordsEntities entities = new EmployeeRecordsEntities())
            {
                entities.Employees.Add(employee);
                entities.SaveChanges();
            }
        }

        public void Put(int id, Employee employee)
        {

            using (EmployeeRecordsEntities entities = new EmployeeRecordsEntities())
            {
                var entity = entities.Employees.FirstOrDefault(e => e.ID == id);

                entity.FirstName = employee.FirstName;
                entity.MiddleName = employee.MiddleName;
                entity.LastName = employee.LastName;

                entities.SaveChanges();
            }
        }

        public void delete(int id)
        {

            using (EmployeeRecordsEntities entities = new EmployeeRecordsEntities())
            {
                 entities.Employees.Remove(entities.Employees.FirstOrDefault(e => e.ID == id));

                entities.SaveChanges();
            }
        }
    }
}
