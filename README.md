Employee Management System Rest API

Create Employee
End Point- http://localhost:49985/api/Employee
Method -POST
 {
     
        "FirstName": "Prince",
        "MiddleName": "kumar",
        "LastName": "Dey"
}

Get  Employees
End Point- http://localhost:49985/api/Employee
Method -Get
[
    {
        "ID": 1,
        "FirstName": "Al",
        "MiddleName": "Sharif",
        "LastName": "Kenan"
    },
    {
        "ID": 3,
        "FirstName": "Prince",
        "MiddleName": "kumar",
        "LastName": "Dey"
    },
    {
        "ID": 4,
        "FirstName": "Monirul",
        "MiddleName": "Islam",
        "LastName": "Khan"
    }
]

Create Employee
End Point- http://localhost:49985/api/Employee/1
Method -PUT
 {
     
        "FirstName": "Al",
        "MiddleName": "Sharif",
        "LastName": "Amin"
}

Create Employee
End Point- http://localhost:49985/api/Employee/1
Method -DELETE
 

